var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var asignaturaSchema = new Schema({
    _id: mongoose.Schema.Types.ObjectId,
    nombre: String,
    nhores: String,
    docent: Object,
    alumnos: Array[mongoose.Schema.Types.ObjectId]
});

module.exports = mongoose.model('Asignatura', asignaturaSchema);