var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var docentSchema = new Schema({
    _id: mongoose.Schema.Types.ObjectId,
    nombre: String,
    apellido: String
});

module.exports = mongoose.model('Docent', docentSchema);