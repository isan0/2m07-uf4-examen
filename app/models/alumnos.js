var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var alumnoSchema = new Schema({
    _id: mongoose.Schema.Types.ObjectId,
    nombre: String,
    apellido: String
});

module.exports = mongoose.model('Alumno', alumnoSchema);