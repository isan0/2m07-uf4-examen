var express = require("express");
var path = require("path");
var router = express.Router();
var alumnCntr = require ("../models/alumnos");


router.get("/", async (req, res, next) => {
    res.render("index.pug");
});

/*
router.get("/new", async (req, res, next) => {
    res.render("newAsignatura.pug");
});
*/
router.get("/new", async (req, res, next)=>{
    let alumnos = await alumnCntr.find();
    res.render('newAsignatura.pug', { alumnos });
});


module.exports = router;